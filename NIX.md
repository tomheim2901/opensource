## QUESTIONS

- Donnez moi une explication de ce que sont les [flake nix](https://nixos.wiki/wiki/Flakes)

Un flake est simplement une arborescence source (telle qu'un référentiel Git) contenant un fichier nommé flake.nix qui fournit une interface standardisée aux artefacts Nix tels que des packages ou des modules NixOS. 

- Jetez un œil au fichier flake, et ajouter son contenu dans le rapport en commentant son contenu (n'en faites pas trop non plus):

`cat flakes.nix`

```bash
# SPDX-License-Identifier: AGPL-3.0-or-later
{
  description = "Open source system tp";

  inputs = {   
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =     
    { self
    , nixpkgs
    , flake-utils
    , ...
    }:
    flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {
        inherit system;
      };
      tpPackages = [
        pkgs.go-task
        pkgs.podman
        pkgs.rustfmt
        pkgs.trivy
        pkgs.hadolint
      ];
    in
    {
      devShells.default =
        pkgs.mkShell
          {
            nativeBuildInputs = [
              pkgs.dapr-cli
              pkgs.stdenv
              pkgs.go
              pkgs.glibc.static
            ] ++ tpPackages;
          };
    });
}
```

`description` sert à décrire le flake.

`inputs` est un ensemble d’attribus des dépendances du flake. 

`outputs` est fonction d'un argument qui prend un ensemble d'attributs de toutes les entrées réalisées, et délivre en sortie un autre ensemble d'attributs

`nixConfig` est un ensemble d'attributs de valeurs qui reflètent les valeurs données à nix.conf. Cela peut étendre le comportement normal de l'expérience nix d'un utilisateur en ajoutant une configuration spécifique aux flakes.

`flakes-utils` Le but de flake-utils est de construire une collection de fonctions Nix pures qui ne dépendent pas de nixpkgs, et qui sont utiles dans le contexte de l'écriture d'autres flakes Nix.

`devShells` Les Devshells peuvent être exécutés en tant qu'applications Nix (via nix run ). Cela permet d'exécuter des commandes définies dans votre devshell sans entrer dans une session nix-shell

- Qu'est ce que `nixpkgs` ?

C’est un gestionnaire de paquets, un équivalent à “apt” par exemple. C’est le plus grand dépôt de paquets Nix et de modules NixOS

- A quoi ca sert

Les paquets (NixPkgs) peuvent fonctionner au-dessus de n'importe quelle distribution Linux ou MacOS et permettent à un utilisateur simple, sans privilèges, d'installer des paquets de son choix.

- Pourquoi la syntaxe elle est bizarre ?

## CONTAINERS

configuration de podman 

```bash
mkdir -p ~/.config/containers/
cat <<EOF > ~/.config/containers/containers.conf
[engine]
events_logger = "file"
cgroup_manager = "cgroupfs"
EOF
cat <<EOF > ~/.config/containers/policy.json 
{
  "default": [
    { "type": "insecureAcceptAnything" }
  ],
  "transports": {
    "docker-daemon": {
      "": [
        { "type": "insecureAcceptAnything" }
      ]
    }
  }
}
EOF
```

Ouvrez votre IDE pref et nous allons créer un `Containerfile`:

```bash
Ouvrez votre IDE pref et nous allons créer un Containerfile:

FROM docker.io/alpine:latest

ENV \
    PATH="/root/.cargo/bin:$PATH"

RUN \
  apk add \
    cargo \
  && cargo install tealdeer \
  && tldr --update 

ENTRYPOINT ["tldr"]
CMD []
```

Nous allons maintenant construire notre container:

`podman build -t tldr .`

```bash
tom@DESKTOP-UVEP99O:~/opensource/system/tp$ podman build -t tldr .
STEP 1/5: FROM docker.io/alpine:latest
STEP 2/5: ENV     PATH="/root/.cargo/bin:$PATH"
--> Using cache 5f6f69229f2066feb6d0fb2b00976733e47282085284065e6975a82ca2cb66ef
--> 5f6f69229f2
STEP 3/5: RUN   apk add     cargo   && cargo install tealdeer   && tldr --update
--> Using cache 4486d3642085a4d863cbd068eeaa041bc1e13402caf28261cfc371b211b87bd9
--> 4486d364208
STEP 4/5: ENTRYPOINT ["tldr"]
--> Using cache adce63e6ce0ce21efcde0f2080eca9d00db0b7d9f61e23f579e113cf7f050708
--> adce63e6ce0
STEP 5/5: CMD []
--> Using cache 482e464af39bb56d6da7184257d6571dfa15b0678ff06a872cbfc5c78664a52e
COMMIT tldr
--> 482e464af39
Successfully tagged localhost/tldr:latest
482e464af39bb56d6da7184257d6571dfa15b0678ff06a872cbfc5c78664a52e
```

Cette commande vient de créer une image docker, il s'agit d'une archive contenant l'image système de notre container.
Elle sont publiables, et directement exécutable sur des machines supportant les containers:

`podman run tldr podman`

Cette commande vous donne de la documentation sous forme de guide pratique pour utiliser podman.
Vous pouvez lui demander des précisions:

`podman run tldr podman ps
podman run tldr podman rm`

Jouez avec la cli podman en utilisant tldr pour vous aider. Vous commenterez ce que vous avez fait dans le rapport

  `podman run tldr podman`

```bash
List Podman containers.
  More information: <https://docs.podman.io/en/latest/markdown/podman-ps.1.html>.

  List currently running podman containers:

      podman ps

  List all podman containers (running and stopped):

      podman ps --all

  Show the latest created container (includes all states):

      podman ps --latest

  Filter containers that contain a substring in their name:

      podman ps --filter "name=name"

  Filter containers that share a given image as an ancestor:

      podman ps --filter "ancestor=image:tag"

  Filter containers by exit status code:

      podman ps --all --filter "exited=code"

  Filter containers by status (created, running, removing, paused, exited and dead):

      podman ps --filter "status=status"

  Filter containers that mount a specific volume or have a volume mounted in a specific path:

      podman ps --filter "volume=path/to/directory" --format "table .ID\t.Image\t.Names\t.Mounts"
```

On voit grâce à cette commande tout ce que l’on peut faire avec podman. 

### QUESTIONS

- Faites des recherches, et donnez moi la différence entre le mot clef `Dockefile`: `CMD` et `ENTRYPOINT`

La commande `CMD` 

 Cette commande spécifie une instruction exécutée chaque fois que la commande docker `RUN` est exécutée

La commande `ENTRYPOINT`

La commande docker `ENTRYPOINT` présente des similitudes avec la commande `CMD`, mais pas tout à fait la même.

Lors de l’utilisation de la commande `CMD`, nous pouvons facilement la remplacer en passant des arguments à l’exécution, mais ce n’est pas le cas avec la commande `ENTRYPOINT`.

Par conséquent, `ENTRYPOINT` peut être utilisé pour ne pas remplacer l’instruction de point d’entrée lors de l’exécution.

Nous pouvons affirmer que ces deux commandes sont très similaires ; cependant, leur différence est que la commande `CMD` peut être remplacée lors de l’exécution alors que la commande `ENTRYPOINT` ne le peut pas.

## Qualité et sécurité

`tom@DESKTOP-UVEP99O:~/opensource/system/tp$ hadolint Containerfile`

```bash
Containerfile:1 DL3007 warning: Using latest is prone to errors if the image will ever update. Pin the version explicitly to a release tag
Containerfile:6 DL3018 warning: Pin versions in apk add. Instead of apk add <package> use apk add <package>=<version>
Containerfile:6 DL3019 info: Use the --no-cache switch to avoid the need to use --update and remove /var/cache/apk/* when done installing packages
```

Voici la Containerfile avant correction 

```bash
FROM docker.io/alpine:latest

ENV \
    PATH="/root/.cargo/bin:$PATH"

RUN \
  apk add \
    cargo \
  && cargo install tealdeer \
  && tldr --update

ENTRYPOINT ["tldr"]
CMD []
```

`DL3007 :` Il faut spécifier une version spécifique —> FROM [docker.io/alpine:3.18](http://docker.io/alpine:3.18)

`DL3018 :` Il faut spécifier une version spécifique du RUN —> RUN \

                                                                                            apk add \
                                                                                            cargo=1.72.1-r0 \
                                                                                            && cargo install tealdeer \
                                                                                            && tldr --update

`DL3019 :` Il faut ajouter l’option —no-cache —> RUN \
                                                                                apk --no-cache add \
                                                                                cargo=1.72.1-r0 \
                                                                                && cargo install tealdeer \
                                                                                && tldr --update

Voici le Containerfile après correction 

```bash
FROM docker.io/alpine:3.18

ENV \
    PATH="/root/.cargo/bin:$PATH"

RUN \
  apk --no-cache add \
    cargo=1.72.1-r0 \
  && cargo install tealdeer \
  && tldr --update

ENTRYPOINT ["tldr"]
CMD []
```

`trivy` est un outil qui vient scanner votre image docker pour trouver des vulnérabilités connues. Pour le tp, nous allons l'installer dans le container que nous venons de créer. Cependant ce n'est pas une pratique que je recommande, je conseil plutôt d'installer `trivy` dans votre environment de dev/CICD (avec `nix` par exemple)

Ajoutez trivy dans votre `Containerfile`:

```bash
RUN apk add curl \
    && curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin \
    && trivy --insecure filesystem --exit-code 1 --no-progress /
```

Et reconstruisez votre image.

`Containerfile`

```bash
FROM docker.io/alpine:3.18

ENV \
    PATH="/root/.cargo/bin:$PATH"

RUN \
  apk --no-cache add \
    cargo=1.72.1-r0 \
  && cargo install tealdeer \
  && tldr --update
RUN apk add curl \
    && curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin \
    && trivy --insecure filesystem --exit-code 1 --no-progress /

ENTRYPOINT ["tldr"]
CMD []
```

Essayez de configurer trivy de sorte qu'il ne vous donne pas de faux positifs, et qu'il n'affiche plus les failles impossibles à fixer.

`trivy fs --scanners vuln,secret,config Containerfile`

```bash

2023-10-13T10:06:19.859+0200    INFO    Need to update DB
2023-10-13T10:06:19.859+0200    INFO    DB Repository: ghcr.io/aquasecurity/trivy-db
2023-10-13T10:06:19.859+0200    INFO    Downloading DB...
40.34 MiB / 40.34 MiB [---------------------------------------------------------------------] 100.00% 14.60 MiB p/s 3.0s
2023-10-13T10:06:24.653+0200    INFO    Vulnerability scanning is enabled
2023-10-13T10:06:24.653+0200    INFO    Misconfiguration scanning is enabled
2023-10-13T10:06:24.653+0200    INFO    Need to update the built-in policies
2023-10-13T10:06:24.653+0200    INFO    Downloading the built-in policies...
44.66 KiB / 44.66 KiB [-------------------------------------------------------------------------------] 100.00% ? p/s 0s
2023-10-13T10:06:25.445+0200    INFO    Secret scanning is enabled
2023-10-13T10:06:25.445+0200    INFO    If your scanning is slow, please try '--scanners vuln' to disable secret scanning
2023-10-13T10:06:25.445+0200    INFO    Please see also https://aquasecurity.github.io/trivy/dev/docs/scanner/secret/#recommendation for faster secret detection
2023-10-13T10:06:26.219+0200    INFO    Number of language-specific files: 0
2023-10-13T10:06:26.219+0200    INFO    Detected config files: 1

Containerfile (dockerfile)

Tests: 27 (SUCCESSES: 21, FAILURES: 6, EXCEPTIONS: 0)
Failures: 6 (UNKNOWN: 0, LOW: 1, MEDIUM: 1, HIGH: 4, CRITICAL: 0)

MEDIUM: Specify a tag in the 'FROM' statement for image 'docker.io/alpine'
════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
When using a 'FROM' statement you should use a specific tag to avoid uncontrolled behavior when the image is updated.

See https://avd.aquasec.com/misconfig/ds001
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 Containerfile:1
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
   1 [ FROM docker.io/alpine:latest
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

HIGH: Specify at least 1 USER command in Dockerfile with non-root user as argument
════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
Running containers with 'root' user can lead to a container escape situation. It is a best practice to run containers as non-root users, which can be done by adding a 'USER' statement to the Dockerfile.

See https://avd.aquasec.com/misconfig/ds002
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

HIGH: The instruction 'RUN <package-manager> update' should always be followed by '<package-manager> install' in the same RUN statement.
════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
The instruction 'RUN <package-manager> update' should always be followed by '<package-manager> install' in the same RUN statement.

See https://avd.aquasec.com/misconfig/ds017
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 Containerfile:6-10
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
   6 ┌ RUN \
   7 │   apk add \
   8 │     cargo \
   9 │   && cargo install tealdeer \
  10 └   && tldr --update
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

HIGH: '--no-cache' is missed: apk add     cargo   && cargo install tealdeer   && tldr --update
════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
You should use 'apk add' with '--no-cache' to clean package cached data and reduce image size.

See https://avd.aquasec.com/misconfig/ds025
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 Containerfile:6-10
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
   6 ┌ RUN \
   7 │   apk add \
   8 │     cargo \
   9 │   && cargo install tealdeer \
  10 └   && tldr --update
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

HIGH: '--no-cache' is missed: apk add curl     && curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin     && trivy --insecure filesystem --exit-code 1 --no-progress /
════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
You should use 'apk add' with '--no-cache' to clean package cached data and reduce image size.

See https://avd.aquasec.com/misconfig/ds025
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 Containerfile:11-13
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
  11 ┌ RUN apk add curl \
  12 │     && curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin \
  13 └     && trivy --insecure filesystem --exit-code 1 --no-progress /
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

LOW: Add HEALTHCHECK instruction in your Dockerfile
════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
You should add HEALTHCHECK instruction in your docker container images to perform the health check on running containers.

See https://avd.aquasec.com/misconfig/ds026
────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
```

Faites moi le un autre `Containerfile-debian` en utilisant `debian` comme base et installez y trivy.

```bash
FROM docker.io/debian:latest

ENV \
    PATH="/root/.cargo/bin:$PATH"

RUN apk add git

RUN apk add curl \
    && curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin \
    && trivy --insecure filesystem --exit-code 1 --no-progress /

ENTRYPOINT ["tldr"]

CMD []
```

`trivy fs --scanners vuln,secret,config Containerfile-debian`

```bash
2023-10-13T10:15:56.206+0200    INFO    Vulnerability scanning is enabled
2023-10-13T10:15:56.206+0200    INFO    Misconfiguration scanning is enabled
2023-10-13T10:15:56.206+0200    INFO    Secret scanning is enabled
2023-10-13T10:15:56.206+0200    INFO    If your scanning is slow, please try '--scanners vuln' to disable secret scanning
2023-10-13T10:15:56.206+0200    INFO    Please see also https://aquasecurity.github.io/trivy/dev/docs/scanner/secret/#recommendation for faster secret detection
2023-10-13T10:15:56.227+0200    INFO    Number of language-specific files: 0
2023-10-13T10:15:56.227+0200    INFO    Detected config files: 0
```

Éditez le fichier `flake.nix` et ajoutez y un nouvel output `container` (au meme niveau que `devShells`):

```bash
# Dans la section let, en dessous de tpPackages = [...];
containerImage = pkgs.dockerTools.buildImage {
  name = "tldr-nix";
  copyToRoot = pkgs.buildEnv {
    name = "image-root";
    paths = [ pkgs.bash ] ++ tpPackages;
    pathsToLink = [ "/bin" ];
  };
};

# Dans l'output au dessus de devShells
# Les backages dans nix sont un output spécial qui permet build des:
# - bibliothèques
# - applications
# - scripts 
# - containers
packages.tp-container = containerImage;
```

Fichier `flake.nix`

```bash
# SPDX-License-Identifier: AGPL-3.0-or-later
{
  description = "Open source system tp";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =
    { self
    , nixpkgs
    , flake-utils
    , ...
    }:
    flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {
        inherit system;
      };
      tpPackages = [
        pkgs.go-task
        pkgs.podman
        pkgs.rustfmt
        pkgs.trivy
        pkgs.hadolint
      ];
  containerImage = pkgs.dockerTools.buildImage {
    name = "tldr-nix";
    copyToRoot = pkgs.buildEnv {
      name = "image-root";
      paths = [ pkgs.bash ] ++ tpPackages;
      pathsToLink = [ "/bin" ];
  };
};
  packages.tp-container = containerImage;
    in
    {
      devShells.default =
        pkgs.mkShell
          {
            nativeBuildInputs = [
              pkgs.dapr-cli
              pkgs.stdenv
              pkgs.go
              pkgs.glibc.static
            ] ++ tpPackages;
          };
    });
}
```

Construisez votre container avec la commande:

`nix build '.#tp-container'`

Notez l’apparition d'un nouveau dossier `result` contenant votre container.

On peut le charger dans podman avec la commande:

`podman load -i result`

Nous venons d'utiliser le `flake` `nix` `dockerTools` qui propose une interface pour construire des images docker.
Cette fonctionnalité permet de déclarer et de composer des configurations d'infrastructures et des environnements de manière déclarative, fournissant une approche plus moderne et flexible pour décrire les dépendances logicielles et les configurations système.

[Ici la documentation du flake `dockerTools`](https://nixos.org/manual/nixpkgs/stable/#sec-pkgs-dockerTools)

Nous allons maintenant lancer notre container avec podman:

`# Changez le tag de l'image par le votrepodman run -it localhost/tldr-nix:v74l0dnfbkdq3x5yshqkh0956wiki5pw bash`

Essayez de lancer plusieurs binaires habituels comme `ls` ou `mkdir` et constatez leur absence.

En effet grace à la gestion explicite et déclarative des dépendances, nous n'héritons plus de la complexité et la multitude d'outils installés par défaut dans les distributions classiques et donc images docker.

Nous avons cependant installé `trivy` dans notre container, et nous pouvons donc l'utiliser:

`trivy --insecure filesystem --exit-code 1 --ignore-unfixed --no-progress /`

On peut voir qu'il ne reste plus que 1 vulnérabilités, et qu'elles sont toutes les deux dans le binaire `bash`.

Et encore, si l'on ajoute l'option `--ignore-unfixed` qui permet d'ignorer les vulnérabilités non corrigées, il ne reste plus aucune vulnérabilité.

Le mode déclaratif, en ne permettant pas de faire de compromis sur les dépendances, permet de garantir la sécurité de nos containers.
C'est une application stricte de 2 bonnes pratiques:

- le principe du droit minimum
- l'évitement

Notez que les applications dans les containers sons soumises à certains parti prix:

- Les logs ne sont pas écrit dans des fichiers mais envoyé sur `stdout`/`stderr`
- Le réseau n'est pas managé par l'application elle meme.
- et bien d'autres
- 

On peut regrouper ses bonnes pratiques sous le nom de [12 factor app](https://12factor.net/fr/)
Renseignez vous dessus et faites moi un court résumé dans votre rapport.

Les 12 factors app sont utilisé pour logiciels en tant que service (software-as-a-service). Voici leur liste : 

### [I. Base de code](https://12factor.net/fr/codebase)

### Une base de code suivie avec un système de contrôle de version, plusieurs déploiements

### [II. Dépendances](https://12factor.net/fr/dependencies)

### Déclarez explicitement et isolez les dépendances

### [III. Configuration](https://12factor.net/fr/config)

### Stockez la configuration dans l’environnement

### [IV. Services externes](https://12factor.net/fr/backing-services)

### Traitez les services externes comme des ressources attachées

### [V. Assemblez, publiez, exécutez](https://12factor.net/fr/build-release-run)

### Séparez strictement les étapes d’assemblage et d’exécution

### [VI. Processus](https://12factor.net/fr/processes)

### Exécutez l’application comme un ou plusieurs processus sans état

### [VII. Associations de ports](https://12factor.net/fr/port-binding)

### Exportez les services via des associations de ports

### [VIII. Concurrence](https://12factor.net/fr/concurrency)

### Grossissez à l’aide du modèle de processus

### [IX. Jetable](https://12factor.net/fr/disposability)

### Maximisez la robustesse avec des démarrages rapides et des arrêts gracieux

### [X. Parité dev/prod](https://12factor.net/fr/dev-prod-parity)

### Gardez le développement, la validation et la production aussi proches que possible

### [XI. Logs](https://12factor.net/fr/logs)

### Traitez les logs comme des flux d’évènements

### [XII. Processus d’administration](https://12factor.net/fr/admin-processes)

### Lancez les processus d’administration et de maintenance comme des one-off-processes

## QUESTIONS

- Expliquez moi ce que sont les `namespaces`, et les `cgroups`.

`Namespaces :` Les *namespaces* permettent d’**isoler le point de vue d’une application de l’environnement** : *process trees network,* les *systèmes de fichiers* et les *user’s ID.* Autrement dit, les *namespaces* permettent de **limiter ce que peut voir l’application**, ou plus généralement ils permettent de **séparer les ressources du noyau** tel qu’un ensemble de processus voit un ensemble de ressources tandis  qu’un autre ensemble de processus verra un ensemble de ressources  différent.

`crgroups :` Les *cgroups* sont une fonctionnalité du noyau Linux qui permettent de **limiter**, **compter et isoler l’utilisation des ressources** (processeur, mémoire, utilisation disques ..).

- Expliquez moi ce qu'est `containerd` ?

Containerd est un **outil permettant d'exécuter des conteneurs compatibles OCI**
Il est conçu pour être un composant central de l'infrastructure de conteneurs et est utilisé par plusieurs systèmes de gestion de conteneurs, dont Docker et Kubernetes.

- Donnez moi les `syscalls` qui sont utilisé pour faire tourner les containers.

Il y a "clone" "namespace" "socket" "exec" "Open, Read, Write, Close" "Chroot" "Cgroup" "Mmap"

- Nous avons utilisé comme source en début de TP un container `alpine`, expliquer en quoi il est intéressant par rapport `debian` ou `ubuntu`.

la taille d’Alpine est très réduite, ce qui permet de la rendre très rapide est peu gourmande pour de la conteneurisation 

- Qu'est ce que `busl` dans la distribution alpine, en quoi est-ce intéressant ?


